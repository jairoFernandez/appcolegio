<?php

namespace Tucolegio\PpalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $respuesta = $this->render('TucolegioPpalBundle:Default:index.html.twig');
        $respuesta->setSharedMaxAge(3600);
        $respuesta->setVary(array('Accept-Encoding', 'Host'));

        return $respuesta;
    }
    
     public function conocenosAction()
    {
        $respuesta = $this->render('TucolegioPpalBundle:Default:conocenos.html.twig');
        $respuesta->setSharedMaxAge(3600);
        $respuesta->setVary(array('Accept-Encoding', 'Host'));
        return $respuesta;
    }
     
    public function contactoAction()
    {
        $respuesta = $this->render('TucolegioPpalBundle:Default:contact.html.twig');
        $respuesta->setSharedMaxAge(3600);
        $respuesta->setVary(array('Accept-Encoding', 'Host'));  
        return $respuesta;
    }
}
