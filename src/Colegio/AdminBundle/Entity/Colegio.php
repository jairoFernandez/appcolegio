<?php

namespace Colegio\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Colegio
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\AdminBundle\Entity\ColegioRepository")
 */
class Colegio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\EstadoColegio")
     */
    private $idEstadoColegio;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\DocenteBundle\Entity\Docente", mappedBy="idColegio", cascade={"persist","remove"})
     * 
     */
    private $docentes;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\TipoColegio")
     */
    private $idTipoColegio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    
    /**
     * Campo q llama a DetalleColegio
     * @ORM\OneToOne(targetEntity="detalleColegio", mappedBy="idColegio", cascade={"persist","remove"})
     */
     private $detalle;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detalle
     * @param \Colegio\AdminBundle\Entity\detalleColegio $detalle
     * @return IdColegio
     */
    public function SetDetalle(\Colegio\AdminBundle\Entity\detalleColegio $detalle = null)
    {
        $this->detalle = $detalle;
        $detalle->setIdColegio($this);
        return $this;
    }

    /**
     * Get detalle
     * 
     * @return \Colegio\AdminBundle\Entity\detalleColegio
     */
    public function GetDetalle()
    {
        return $this->detalle;
        
    }

    /**
     * Set docentes
     *
     * @return \Colegio\DocenteBundle\Entity\Docente
     */
    public function setDocentes(\Doctrine\Common\Collections\Collection $docentes)
    {
        $this->docentes = $docentes;
    	foreach ($docentes as $docente) {
            $docente->setIdColegio($this);
        	}
        return $this;
    }

    /**
     * Get docentes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocentes()
    {
        return $this->docentes;
    }



    /**
     * Set idDireccion
     *
     * @param string $idDireccion
     * @return Colegio
     */
    public function setIdDireccion($idDireccion)
    {
        $this->idDireccion = $idDireccion;
    
        return $this;
    }

    /**
     * Get idEstadoColegio
     *
     * @return string 
     */
    public function getIdEstadoColegio()
    {
        return $this->idEstadoColegio;
    }
    
    /**
     * Set idEstadoColegio
     *
     * @param string $idEstadoColegio
     * @return Colegio
     */
    public function setIdEstadoColegio(\Colegio\AdminBundle\Entity\EstadoColegio $idEstadoColegio)
    {
        $this->idEstadoColegio = $idEstadoColegio;
    
        return $this;
    }
    /**
     * Set idTipoColegio
     *
     * @param string $idTipoColegio
     * @return Colegio
     */
    public function setIdTipoColegio(\Colegio\AdminBundle\Entity\TipoColegio $idTipoColegio)
    {
        $this->idTipoColegio = $idTipoColegio;
    
        return $this;
    }

    /**
     * Get idTipoColegio
     *
     * @return string 
     */
    public function getIdTipoColegio()
    {
        return $this->idTipoColegio;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Colegio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Colegio
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Colegio
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->docentes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add docentes
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $docentes
     * @return Colegio
     */
    public function addDocente(\Colegio\DocenteBundle\Entity\Docente $docentes)
    {
        $this->docentes[] = $docentes;
    
        return $this;
    }

    /**
     * Remove docentes
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $docentes
     */
    public function removeDocente(\Colegio\DocenteBundle\Entity\Docente $docentes)
    {
        $this->docentes->removeElement($docentes);
    }
}