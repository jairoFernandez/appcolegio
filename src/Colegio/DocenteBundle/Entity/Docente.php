<?php

namespace Colegio\DocenteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Docente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\DocenteBundle\Entity\DocenteRepository")
 */
class Docente implements UserInterface, \Serializable
{
    function eraseCredentials()
    {
    }
    function getRoles()
    {
        return array('ROLE_DOCENTE');
    }
    
    function getUsername()
    {
        return $this->getEmail();
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;
    
     /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Sede")
     */
    private $idSede;

     /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio", inversedBy="docentes", cascade={"persist","remove"})
     */
    private $idColegio;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Asignatura")
     */
    private $idMateria;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Nombres", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     * @Assert\Email(message="No es un email válido")
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Usuarios\UsuariosBundle\Entity\TipoIdentificacion")
     */
    private $idTipoIdentificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroIdentificacion", type="string", length=255)
     */
    private $numeroIdentificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;


    /**
     * @ORM\OneToMany(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", mappedBy="idDocente", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $gruposAsignaturas;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gruposAsignaturas = new ArrayCollection();
    }

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Docente
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }
    
    
     /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Docente
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }
    
    
    
    
    /**
     * Set idSede
     *
     * @param string $idSede
     * @return Docente
     */
    public function setIdSede(\Colegio\AdminBundle\Entity\Sede $idSede)
    {
        $this->idSede = $idSede;
    
        return $this;
    }

    
    /**
     * Get idSede
     *
     * @return string 
     */
    public function getIdSede()
    {
        return $this->idSede;
    }

    /**
     * Set idColegio
     *
     * @param string $idColegio
     * @return Docente
     */
    public function setIdColegio(\Colegio\AdminBundle\Entity\Colegio $idColegio)
    {
        $this->idColegio = $idColegio;
    
        return $this;
    }

    /**
     * Get idColegio
     *
     * @return string 
     */
    public function getIdColegio()
    {
        return $this->idColegio;
    }
    
    /**
     * Set idMateria
     *
     * @param string $idMateria
     * @return Docente
     */
    public function setIdMateria(\Colegio\GrupoBundle\Entity\Asignatura $idMateria)
    {
        $this->idMateria = $idMateria;
    
        return $this;
    }

    /**
     * Get idMateria
     *
     * @return string 
     */
    public function getIdMateria()
    {
        return $this->idMateria;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Docente
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
    
        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Docente
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Docente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Docente
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set idTipoIdentificacion
     *
     * @param string $idTipoIdentificacion
     * @return Docente
     */
    public function setIdTipoIdentificacion(\Usuarios\UsuariosBundle\Entity\TipoIdentificacion $idTipoIdentificacion)
    {
        $this->idTipoIdentificacion = $idTipoIdentificacion;
    
        return $this;
    }

    /**
     * Get idTipoIdentificacion
     *
     * @return string 
     */
    public function getIdTipoIdentificacion()
    {
        return $this->idTipoIdentificacion;
    }

    /**
     * Set numeroIdentificacion
     *
     * @param string $numeroIdentificacion
     * @return Docente
     */
    public function setNumeroIdentificacion($numeroIdentificacion)
    {
        $this->numeroIdentificacion = $numeroIdentificacion;
    
        return $this;
    }

    /**
     * Get numeroIdentificacion
     *
     * @return string 
     */
    public function getNumeroIdentificacion()
    {
        return $this->numeroIdentificacion;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Docente
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    /**
     * Set gruposAsignaturas
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function setGruposAsignaturas(ArrayCollection $gruposAsignaturas)
    {
        $this->gruposAsignaturas = $gruposAsignaturas;
        foreach ($gruposAsignaturas as $grupoasigantura) {
            $grupoasigantura->setIdDocente($this);
        }
    }

    /**
     * Get gruposAsignaturas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGruposAsignaturas()
    {
        return $this->gruposAsignaturas;
    }

    
    public function __toString()
    {
        return $this->getNombres()." ".$this->getApellidos();
    }
    
    /**
    * @see \Serializable::serialize()
    */
    public function serialize()
    {
        return serialize(array(
        $this->id,
        
        $this->salt,
        $this->password,
        ));
    }
    /**
    * @see \Serializable::unserialize()
    */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            
            $this->salt,
            $this->password,
            ) = unserialize($serialized);    
    }    
}
