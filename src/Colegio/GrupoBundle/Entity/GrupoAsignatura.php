<?php

namespace Colegio\GrupoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GrupoAsignatura
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\GrupoBundle\Entity\GrupoAsignaturaRepository")
 */
class GrupoAsignatura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Grupo", inversedBy="gruposAsignaturas", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="idGrupo_id", referencedColumnName="id")
     */
    private $idGrupo;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\GrupoBundle\Entity\Asignatura", inversedBy="asignaturasGrupos", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="idAsignatura_id", referencedColumnName="id")
     */
    private $idAsignatura;

    /**
     * @ORM\ManyToOne(targetEntity="Colegio\DocenteBundle\Entity\Docente", inversedBy="gruposAsignaturas", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="idDocente_id", referencedColumnName="id")
     */
    private $idDocente;

    /**
     * @ORM\ManyToMany(targetEntity="Colegio\BoletinBundle\Entity\Boletin", inversedBy="idGrupoAsignatura")
     * @0RM\JoinTable(name="grupos_boletines",
     *      joinColumns={@JoinColumn(name="boletines_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="idGrupoAsignatura_id", referencedColumnName="id")}
     *      )
     */
    private $boletines;
    
    public function __construct() 
    {
        $this->boletines = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGrupo
     *
     * @param \Colegio\GrupoBundle\Entity\Grupo $idGrupo
     * @return GrupoAsignatura
     */
    public function setIdGrupo(\Colegio\GrupoBundle\Entity\Grupo $idGrupo)
    {
        $this->idGrupo = $idGrupo;
    
        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \Colegio\GrupoBundle\Entity\Grupo
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set idAsignatura
     *
     * @param \Colegio\GrupoBundle\Entity\Asignatura $idAsignatura
     * @return GrupoAsignatura
     */
    public function setIdAsignatura(\Colegio\GrupoBundle\Entity\Asignatura $idAsignatura)
    {
        $this->idAsignatura = $idAsignatura;
    
        return $this;
    }

    /**
     * Get idAsignatura
     *
     * @return \Colegio\GrupoBundle\Entity\Asignatura
     */
    public function getIdAsignatura()
    {
        return $this->idAsignatura;
    }

    /**
     * Set idDocente
     *
     * @param \Colegio\DocenteBundle\Entity\Docente $idDocente
     * @return GrupoAsignatura
     */
    public function setIdDocente(\Colegio\DocenteBundle\Entity\Docente $idDocente)
    {
        $this->idDocente = $idDocente;
    
        return $this;
    }

    /**
     * Get idDocente
     *
     * @return \Colegio\DocenteBundle\Entity\Docente 
     */
    public function getIdDocente()
    {
        return $this->idDocente;
    }
    
    public function __toString() 
    {
        return $this->getIdGrupo().' - '.$this->getIdAsignatura();
    }
}
