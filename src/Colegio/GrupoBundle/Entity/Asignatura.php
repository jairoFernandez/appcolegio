<?php

namespace Colegio\GrupoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Asignatura
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Colegio\GrupoBundle\Entity\AsignaturaRepository")
 */
class Asignatura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\BoletinBundle\Entity\Boletin", mappedBy="asignatura", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $asignaturasboletin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Colegio\GrupoBundle\Entity\GrupoAsignatura", mappedBy="idAsignatura", cascade={"persist","remove"})
     * @Assert\Valid()    
     */
    private $asignaturasGrupos;
  
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Colegio\AdminBundle\Entity\Colegio")
     */
    private $idColegio;    

    
    /**
     * Set asignaturasGrupos
     *
     * @return \Colegio\GrupoBundle\Entity\GrupoAsignatura
     */
    public function setAsignaturasGrupos(ArrayCollection $asignaturasGrupos)
    {
        $this->asignaturasGrupos = $asignaturasGrupos;
        foreach ($asignaturasGrupos as $asiganturagrupo) {
            $asignaturagrupo->setIdAsignatura($this);
        }
    }

    /**
     * Get asignaturasGrupos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsignaturasGrupos()
    {
        return $this->asignaturasGrupos;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->asignaturasGrupos = new ArrayCollection();
    }

    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Asignatura
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set idColegio
     *
     * @param string $nombre
     * @return Asignatura
     */
    public function setIdColegio(\Colegio\AdminBundle\Entity\Colegio $idColegio)
    {
        $this->idColegio = $idColegio;
    
        return $this;
    }

    /**
     * Get idColegio
     *
     * @return string 
     */
    public function getIdColegio()
    {
        return $this->idColegio;
    }
    
    
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
